### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying Slide, an open source, ad free Reddit browser for Android.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/ccrama/Slide).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/slide/-/compare/master...codavel?from_project_id=29759213)):

1. Added maven repository and Codavel's Application ID and Secret to the app's build.gradle, required to download and use Codavel's SDK
2. Start Codavel's Service when the main activity is created, so that our SDK can start processing the HTTP requests.
3. Change the app's Universal Image Loader configuration to use an OkHTTPClient registered with our HTTP interceptor, so that all the HTTP requests executed through the app's Universal Image Loader are processed and forwarded to our SDK.
